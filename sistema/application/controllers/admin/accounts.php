<?php

error_reporting(E_ALL); ini_set('display_errors', 1);

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/*
 *	@author : CodesLab
 *  @support: support@codeslab.net
 *	date	: 05 June, 2015
 *	Easy Inventory
 *	http://www.codeslab.net
 *  version: 1.0
 */

class Accounts extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('accounts_model');
    }


    /*** List of accounts payable ***/
    public function payable()
    {

        $data['menu'] = array('user_role' => 1, 'c_user_role' => 1);
        $data['title'] = 'Cuentas por pagar';

        $this->accounts_model->_table_name = 'tbl_accounts';
        $this->accounts_model->_order_by = 'id';

        $data['all_accounts_payable'] = $this->accounts_model->get_all_payable();
        //print_r($data['all_accounts_payable']);

        $data['subview'] = $this->load->view('admin/accounts/payable', $data, true);
        $this->load->view('admin/_layout_main', $data);
    }

    /*** List of accounts receivable ***/
    public function receivable()
    {

        $data['menu'] = array('user_role' => 1, 'c_user_role' => 1);
        $data['title'] = 'Cuentas por cobrar';

        $this->accounts_model->_table_name = 'tbl_accounts';
        $this->accounts_model->_order_by = 'id';

        $data['all_accounts_receivable'] = $this->accounts_model->get_all_receivable();
        //print_r($data['all_accounts_receivable']);

        $data['subview'] = $this->load->view('admin/accounts/receivable', $data, true);
        $this->load->view('admin/_layout_main', $data);
    }


    /*** Add an account payable ***/
    public function add_account_payable($id = null)
    {
        $this->tbl_accounts('id');

        if ($id) {
            $data['account'] = $this->global_model->get_by(array('id'=>$id), true);
            if(empty($data['account'])){
                $type = 'error';
                $message = 'There is no Record Found!';
                set_message($type, $message);
                redirect('admin/accounts/payable');
            }
        }

        $this->db->select_max('id');
        $lastId = $this->db->get('tbl_accounts')->row()->id;
        
        //como son cuentas por pagar obtengo los proveedores
        $data['supplier'] = $this->accounts_model->get_suppliers();
        
        //$data['code'] = $customerNo = 100 + $lastId + 1;
        $data['title'] = 'Agregar cuenta por pagar';  // title page
        //$data['editor'] = $this->data;
        $data['subview'] = $this->load->view('admin/accounts/add_account_payable', $data, true);
        $this->load->view('admin/_layout_main', $data);
    }

    /*** Add an account payable ***/
    public function add_account_receivable($id = null)
    {
        //echo "EL ID ES: ".$id;
        //die();
        $this->tbl_accounts('id');

        if ($id) {
            $data['account'] = $this->global_model->get_by(array('id'=>$id), true);
            if(empty($data['account'])){
                $type = 'error';
                $message = 'There is no Record Found!';
                set_message($type, $message);
                redirect('admin/accounts/payable');
            }
        }

        /*
        echo "<br>DATA 1652:<br>";
        print_r($data['account']);
        */

        $this->db->select_max('id');
        $lastId = $this->db->get('tbl_accounts')->row()->id;

        
        //como son cuentas por cobrar obtengo los clientes
        $data['customers'] = $this->accounts_model->get_customers();
        
        //$data['code'] = $customerNo = 100 + $lastId + 1;
        $data['title'] = 'Agregar cuenta por cobrar';  // title page
        $data['editor'] = $data;
        $data['subview'] = $this->load->view('admin/accounts/add_account_receivable', $data, true);
        $this->load->view('admin/_layout_main', $data);
    }

    /*** Save account payable ***/
    public function save_account_payable($id = null)
    {
        $this->db->select_max('id');
        $lastId = $this->db->get('tbl_accounts')->row()->customer_id;

        $data = $this->accounts_model->array_from_post(
            array(
                'is_payable',
                'supplier_customer_id',
                'value',
                'date_due',
                'notes'
            ));

        //log_message('error', $data['notes']."|".$data['supplier_customer_id']."|".$data['value']."|".$data['date_due']);

        $this->tbl_accounts('id');
        $id = $this->global_model->save($data, $id);

        
        if(empty($id)) {
            $this->global_model->save($customer_code, $customer_id);
        }        

        $type = 'success';
        $message = 'La nueva cuenta se guardó correctamente:';
        set_message($type, $message);
        redirect('admin/accounts/payable');
    }

    /*** Save account receivable ***/
    public function save_account_receivable($id = null)
    {
        
        $this->db->select_max('id');
        $lastId = $this->db->get('tbl_accounts')->row()->customer_id;

        $data = $this->accounts_model->array_from_post(
            array(
                'is_payable',
                'supplier_customer_id',
                'value',
                'date_due',
                'notes'
            ));

        //log_message('error', $data['notes']."|".$data['supplier_customer_id']."|".$data['value']."|".$data['date_due']);

        $this->tbl_accounts('id');
        $id = $this->global_model->save($data, $id);

        
        if(empty($id)) {
            $this->global_model->save($customer_code, $customer_id);
        }        

        $type = 'success';
        $message = 'La nueva cuenta se guardó correctamente:';
        set_message($type, $message);
        redirect('admin/accounts/receivable');
    }


    /*** Delete account ***/
    public function delete_account($id = null)
    {
        //die("TRYING TO DELETE:"+$id);
        if (!empty($id)) {
            
            //delete procedure run
            // Check employee in db or not
            $this->accounts_model->_table_name = 'tbl_accounts'; //table name
            $this->accounts_model->_order_by = 'id';
            $result = $this->accounts_model->get_by(array('id' => $id), true);

            if (count($result)) {
                $this->db->where('id =', $id);
                $this->db->delete('tbl_accounts');
                //redirect successful msg
                $type = 'success';
                $message = 'La cuenta se borró correctamente';
                set_message($type, $message);
                if ($result->is_payable){
                    redirect('admin/accounts/payable');
                } else {
                    redirect('admin/accounts/receivable');
                }
                 //redirect page
            } else {
                //redirect error msg
                $type = 'error';
                $message = 'ERROR: este registro no existe';
                set_message($type, $message);
                if ($result->is_payable){
                    redirect('admin/accounts/payable');
                } else {
                    redirect('admin/accounts/receivable');
                }
            }

        }
    }

    
}
