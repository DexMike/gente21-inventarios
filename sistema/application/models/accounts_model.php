<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/*
 *	@author : CodesLab
 *  @support: support@codeslab.net
 *	date	: 05 June, 2015
 *	Easy Inventory
 *	http://www.codeslab.net
 *  version: 1.0
 */

class Accounts_Model extends MY_Model
{
    public $_table_name;
    public $_order_by;
    public $_primary_key;


    public function get_all_payable()
    { // get category info by category name from tbl_subcategory with join tbl_category by root category id
        $this->db->select('tbl_accounts.*', false);
        $this->db->select('tbl_supplier.company_name AS company', false);
        $this->db->from('tbl_accounts');
        $this->db->join('tbl_supplier', 'tbl_supplier.supplier_id = tbl_accounts.supplier_customer_id', 'left');
        //this is for payable
        $this->db->where('tbl_accounts.is_payable', 1);
        $this->db->order_by('date_due', 'ASC');
        
        $query_result = $this->db->get();
        //print_r($query_result);
        $result = $query_result->result();

        return $result;
    }

    public function get_all_receivable()
    { // get category info by category name from tbl_subcategory with join tbl_category by root category id
        $this->db->select('tbl_accounts.*', false);
        $this->db->select('tbl_customer.customer_name AS company', false);
        $this->db->from('tbl_accounts');
        $this->db->join('tbl_customer', 'tbl_customer.customer_id = tbl_accounts.supplier_customer_id', 'left');
        //this is for receivable
        $this->db->where('tbl_accounts.is_payable', 0);
        $this->db->order_by('date_due', 'ASC');
        
        $query_result = $this->db->get();
        //print_r($query_result);
        $result = $query_result->result();

        return $result;
    }

    public function get_suppliers()
    { // get category info by category name from tbl_subcategory with join tbl_category by root category id
        $this->db->select('tbl_supplier.supplier_id, tbl_supplier.company_name, tbl_supplier.supplier_name', false);
        $this->db->from('tbl_supplier');
        $this->db->order_by('tbl_supplier.company_name', 'ASC');
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function get_customers()
    { // get category info by category name from tbl_subcategory with join tbl_category by root category id
        $this->db->select('tbl_customer.customer_id, tbl_customer.customer_name', false);
        $this->db->from('tbl_customer');
        $this->db->order_by('tbl_customer.customer_name', 'ASC');
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    


}
