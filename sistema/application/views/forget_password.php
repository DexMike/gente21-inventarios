<?php $this->load->view('admin/components/header'); ?>
<link href="<?php echo base_url(); ?>asset/css/animation.css" rel="stylesheet">

<body class="login-page">


<div class="login-box retrive-password">
    <div class="login-logo animated fadeInDown" data-animation="fadeInDown">
        <a href="<?php echo base_url() ?>"><img src="<?php echo base_url() ?>/public/img/logo_login.png" alt="GoAdmin"></a>
    </div><!-- /.login-logo -->

    <div class="login-box-body  animated fadeInUp" data-animation="fadeInUp">



        <div class="panel panel-default">
            <div class="panel-heading">Recuperar Contraseña</div>
            <div class="panel-body">

<!--                <form method="post" action="--><?php //echo base_url() ?><!--forget_password/retrieve_password">-->

                    <?php echo $this->session->flashdata('error'); ?>
                    <?php echo form_open('forget_password/retrieve_password') ?>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Nombre de Usuario</label>
                        <input type="text" class="form-control" name="username" placeholder="Nombre de Usuario" required>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Correo Electrónico</label>
                        <input type="email" class="form-control" name="email" placeholder="Ingrese su Correo Electrónico" required>
                    </div>

                    <button type="submit" class="btn bg-navy btn-block">Recuperar Contraseña</button>

                <?php echo form_close() ?>
                <br/>
                <a href="<?php echo base_url() ?>">Ir a la página de Ingreso</a>

            </div>
        </div>



    </div><!-- /.login-box-body -->
</div><!-- /.login-box -->

</body>
</html>