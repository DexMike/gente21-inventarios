
<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<?php setlocale(LC_MONETARY, "en_US.utf-8"); ?>

<section class="content">
    <div class="row">
        <div class="col-md-12">

            <div class="box box-primary ">
                <div class="box-header box-header-background with-border">
                    <h3 class="box-title ">Cuentas por cobrar</h3>
                </div>


                <div class="box-body">

                    <!-- Table -->
                    <table class="table table-bordered table-striped" id="dataTables-example">
                        <thead ><!-- Table head -->
                        <tr>
                            <th class="col-sm-1 active">ID</th>
                            <th class="active">Cliente</th>
                            <th class="col-sm-2 active">Fecha recibida</th>
                            <th class="col-sm-1 active">Cantidad</th>
                            <th class="col-sm-2 active">Vencimiento</th>
                            <th class="col-sm-2 active">Notas</th>
                            <th class="col-sm-1 active">Acción</th>

                        </tr>
                        </thead><!-- / Table head -->
                        <tbody>

                        <?php $key = 1 ?>
                        <?php if (count($all_accounts_receivable)): foreach ($all_accounts_receivable as $accounts_receivable) : ?>

                            <tr>
                                <td><?php echo $accounts_receivable->id ?></td>
                                <td><?php echo $accounts_receivable->company ?></td>
                                <td><?php echo date("d/m/Y", strtotime($accounts_receivable->date_entered)) ?></td>
                                <td><?php echo $this->localization->currencyFormat($accounts_receivable->value) ?></td>
                                <td><strong><?php 

                                    echo date("d/m/Y", strtotime($accounts_receivable->date_due));

                                    if( strtotime($accounts_receivable->date_due) < strtotime('+7 day') ) {
                                        echo '<i class="fa fa-exclamation-triangle" style="color:red" title="Vence esta semana"></i>';
                                    } else if ( strtotime($accounts_receivable->date_due) <= strtotime('+15 day') ) {
                                        echo '<i class="fa fa-exclamation-triangle" style="color:orange" title="Vence en un periodo de 15 días"></i>';
                                    }

                                ?></strong></td>

                                <td><?php echo $accounts_receivable->notes ?></td>
                                
                                <!--<td><?php //echo $accounts_receivable->flag == 1 ? 'Admin' : 'User' ?></td>
                                -->
                                <td>
                                    <?php echo btn_edit('admin/accounts/add_account_receivable/' . $accounts_receivable->id); ?>
                                    <?php echo btn_delete('admin/accounts/delete_account/' . $accounts_receivable->id); ?>
                                </td>
                                

                            </tr>
                            <?php
                            $key++;
                        endforeach;
                            ?>
                        <?php else : ?>
                            <td colspan="3">
                                <strong>No hay datos para mostrar</strong>
                            </td>
                        <?php endif; ?>
                        </tbody><!-- / Table body -->
                    </table> <!-- / Table -->

                </div><!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!--/.col end -->
    </div>
    <!-- /.row -->
</section>




