 <!-- View massage -->
 <?php error_reporting(E_ALL); ini_set('display_errors', 1); ?>
<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>

<section class="content">
    <div class="row">
        <div class="col-md-12">

            <div class="box box-primary">
                <div class="box-header box-header-background with-border">
                  <h3 class="box-title ">Agregar cuenta por cobrar</h3>
                </div>
                <!-- /.box-header -->

                <!-- form start -->
                <form role="form" enctype="multipart/form-data" id="addCustomerForm"
                      action="<?php echo base_url(); ?>admin/accounts/save_account_receivable/<?php if (!empty($account->id)) {
                          echo $account->id;
                      } ?>"
                      method="post">

                    <div class="row">

                        <div class="col-md-8 col-sm-12 col-xs-12">

                            <div class="box-body">

                                <!-- /.proveedor -->
                                <div class="form-group">
                                  <label>Cliente</label>                                  
                                  <select class="form-control" name="supplier_customer_id" required>

                                      <option value=""><strong>-- Seleccione el cliente --</strong></option>

                                      <?php foreach($customers as $v_customer):?>
                                          <option value="<?php echo $v_customer->customer_id; ?>"
                                            <?php if (!empty($account->supplier_customer_id) && $account->supplier_customer_id == $v_customer->customer_id){
                                              echo "selected";
                                              } ?>
                                          ><?php echo $v_customer->customer_name; ?></option>
                                      <?php endforeach; ?>
                                  </select>
                                </div>

      
                                <!-- /.cantidad -->
                                <div class="form-group">
                                    <label">Cantidad<span class="required">*</span></label>
                                    <input type="text" name="value" placeholder="Cantidad" value="<?php
                                           if (!empty($account->value)) {
                                               echo $account->value;
                                           }
                                           ?>" class="form-control">
                                </div>


                                <!-- /.vencimieno -->
                                <div class="form-group">
                                    <label>
                                      Fecha de vencimiento <span
                                            class="required">*</span></label>
                                            <?php 
                                              //clean up the date
                                              if (!empty($account->date_due)) {
                                                $start_date = str_replace('-', '/', $account->date_due);
                                                $date_prefix = explode(' ', $start_date);
                                              }
                                            ?>

                                    <input type="text" value="<?php
                                           if (!empty($account->date_due)) {
                                               echo $date_prefix[0];
                                           }
                                           ?>" 
                                      class="form-control datepicker" 
                                      id="date_due" name="date_due" data-format="yyyy/mm/dd">
                                </div>


                                <!-- /.observaciones -->
                                <div class="form-group">
                                    <label>Observaciones<span class="required">*</span></label>
                                    <textarea name="notes" placeholder="Observaciones" class="form-control"><?php
                                           if (!empty($account->notes)) {
                                               echo $account->notes;
                                           }
                                           ?></textarea>
                                </div>


                                <input type="hidden" name="is_payable" value="0">


                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>

                    <div class="box-footer">
                        <button type="submit" id="customer_btn" class="btn bg-navy btn-flat" type="submit">
                          Agregar una cuenta
                        </button>
                    </div>
                </form>
            </div>
            <!-- /.box -->
        </div>
        <!--/.col end -->
    </div>
    <!-- /.row -->
</section>

<script src="<?php echo base_url() ?>asset/js/ajax.js" ></script>