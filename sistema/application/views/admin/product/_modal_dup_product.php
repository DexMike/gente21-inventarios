 
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
    <h4 class="modal-title" id="myModalLabel">Duplicar el producto: <?php echo $product->product_name." (".$product->product_code.")"; ?></h4>
</div>
<div class="modal-body wrap-modal wrap" style="max-height: 900px;">

<?php //print_r($product) ?>

    <div class="row">
        <div class="col-sm-6 col-md-4">
            <div class="row">
                <div class="col-md-12">
                    <div class="product-thumbnail">
                        <?php if(!empty($product->filename)){?>
                            <img src="<?php echo base_url() . $product->filename; ?>" class="img-circle" alt="Product Image"/>
                        <?php }else{?>
                            <img src="<?php echo base_url(); ?>img/product.png" class="img-circle" alt="Product Image"/>
                        <?php } ?>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="product-barcode">
                        <img src="<?php echo base_url() . $product->barcode ?>" />
                    </div>
                </div>
            </div>
        </div>

        <!-- CLIENT SPECIFIC -->
        <?php if ($this->config->config['clientID'] == 'portoBlanco'){
            include_once('application/clients/'.$this->config->config['clientID'].'/views/seleccionar_talla_titulo.php');
        } ?>

        <div class="col-sm-6 col-md-8">

            <form role="form" enctype="multipart/form-data" id="addProductForm" onsubmit="return imageForm(this)"
                      action="<?php echo base_url(); ?>admin/product/dup_product/<?php echo $product_id ?>" method="post">

                <!-- CLIENT SPECIFIC -->
                <?php if ($this->config->config['clientID'] == 'portoBlanco'){
                    include_once('application/clients/'.$this->config->config['clientID'].'/views/seleccionar_talla_select.php');
                } else { ?>

                <div class="row">
                    <div class="col-md-4">
                        <label>Clave del producto:</label>
                    </div>
                    <div class="col-md-8">
                        <input name="productsize" type="text" placeholder="Clave del producto (SKU)">
                    </div>
                </div>

                <?php } ?>

                <div class="row">
                    <div class="col-md-4">
                        <label>Cantidad inicial:</label>
                    </div>
                    <div class="col-md-8">
                        <input name="cantidadinicial" type="number" value="1">
                    </div>
                </div>

                <br><br><br>

                <div class="row">
                    <button type="submit"  id="submit" class="btn bg-navy btn-flat col-md-offset-3" type="submit">
                        Duplicar ahora
                    </button>
                </div>

            </form>

        </div>


    </div>


    <div class="modal-footer" >

        <!--
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancelar</button>
        <a href="" type="button" class="btn btn-primary">Duplicar ahora</a>
        -->

    </div>

</div>


