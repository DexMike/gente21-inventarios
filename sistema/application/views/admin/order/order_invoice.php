

<!--Massage-->
<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<!--/ Massage-->
<div class="box">
    <div class="box-header box-header-background with-border">
        <h3 class="box-title">Factura de la orden</h3>
        <div class="box-tools pull-right">
            <!-- Buttons, labels, and many other things can be placed here! -->
            <!-- Here is a label for example -->
            <div class="box-tools">
                <div class="btn-group" role="group" >
                    <a onclick="print_invoice('printableArea')" class="btn btn-default ">Impresión</a>
                    <a href="<?php echo base_url() ?>admin/order/pdf_invoice/<?php echo $invoice_info->invoice_no ?>" class="btn btn-default ">PDF</a>
                    <a href="<?php echo base_url() ?>admin/order/email_invoice/<?php echo $invoice_info->invoice_no ?>" class="btn btn-default " <?php
                    echo $order_info->customer_email == '' ? 'disable':''
                    ?>>Correo electrónico al cliente</a>
                </div>
            </div>

        </div><!-- /.box-tools -->
    </div><!-- /.box-header -->
    <div class="box-body">


        <div id="printableArea">
            <link href="<?php echo base_url(); ?>asset/css/invoice.css" rel="stylesheet" type="text/css" />



            <div class="row ">


            <div class="col-md-10 col-md-offset-1">

                <header class="clearfix">
                    <div id="logo">
                        <img src="<?php echo $this->localization->profile('logo') ?>">
                    </div>
                    <div id="invoice">
                        <h3 style="color: #00a7d0">FACTURA <?php echo $invoice_info->invoice_no ?></h3>
                        <div class="date">Fecha de la factura: <?php echo $this->localization->dateFormat($invoice_info->invoice_date) ?></div>
                        <div class="date">Método de pago: <?php echo  ucwords($order_info->payment_method) ?></div>
                        <?php if(!empty($order_info->payment_ref)){ ?>
                            <div class="date">Referencia de pago: <?php echo $order_info->payment_ref ?></div>
                        <?php }?>



                    </div>

                </header>
                <hr/>
                <main>
                    <div id="details" class="clearfix">
                        <div id="client" style="margin-right: 100px">
                            <div class="to"><strong>INFORMACIÓN DE FACTURACIÓN DEL CLIENTE:</strong></div>
                            <h2 class="name"><?php echo $order_info->customer_name ?></h2>
                            <div class="address"><?php echo $order_info->customer_address ?></div>
                            <div class="address"><?php echo $order_info->customer_phone ?></div>
                            <div class="email"><?php echo $order_info->customer_email ?></div>
                        </div>
                        <?php if(!empty($order_info->shipping_address)):?>
                        <div id="shipping">
                            <div class="to"><strong>INFORMACIÓN DE ENVÍO DEL CLIENTE:</strong></div>

                            <div class="address"><?php
                                echo  nl2br($order_info->shipping_address);
                                ?></div>

                        </div>
                        <?php endif ?>


                    </div>


                    <?php if(!empty($order_info->note)){ ?>
                    <table width="100%" border="0" cellspacing="0" cellpadding="5">
                        <tr>
                            <td>

                                <div><strong>Nota de pedido:</strong></div>
                                <?php echo $order_info->note ?>

                            </td>
                        </tr>
                    </table>
                    <?php }?>

                    <table border="0" cellspacing="0" cellpadding="0">
                        <thead>
                        <tr>
                            <th class="no text-right">#</th>
                            <th class="desc">DESCRIPCIÓN</th>
                            <th class="unit text-right">Precio Unitario</th>
                            <th class="qty text-right">CANTIDAD</th>
                            <th class="total text-right ">TOTAL</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $counter = 1?>
                        <?php foreach($order_details as $v_order): ?>
                        <tr>
                            <td class="no"><?php echo $counter ?></td>
                            <td class="desc"><h3><?php echo $v_order->product_name ?></h3></td>
                            <td class="unit"><?php echo $this->localization->currency($v_order->selling_price); ?></td>
                            <td class="qty"><?php echo $v_order->product_quantity ?></td>
                            <td class="total"><?php echo $this->localization->currency($v_order->sub_total) ?></td>
                        </tr>
                            <?php $counter ++?>
                        <?php endforeach; ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="2"></td>
                            <td colspan="2">SUBTOTAL</td>
                            <td align="right"><?php echo $this->localization->currency($order_info->sub_total) ?></td>
                        </tr>

                        <tr>
                            <td colspan="2"></td>
                            <td colspan="2">Impuesto</td>
                            <td><?php echo $this->localization->currency($order_info->total_tax) ?></td>
                        </tr>

                        <?php if($order_info->discount):?>
                            <tr>
                                <td colspan="2"></td>
                                <td colspan="2">Importe de descuento</td>
                                <td><?php echo $this->localization->currency($order_info->discount_amount) ?></td>
                            </tr>
                        <?php endif; ?>

                        <tr>
                            <td colspan="2"></td>
                            <td colspan="2">TOTAL</td>
                            <td><?php echo $this->localization->currencyFormat($order_info->grand_total) ?></td>
                        </tr>
                        </tfoot>
                    </table>

                    <br/>
                    <br/>
                    <br/>

                        <div class="date pull-left"><strong>Persona de ventas:</strong> <?php echo $order_info->sales_person ?></div>


                    <br/>

                </main>
                <hr/>
                <footer class="text-center">
                    <strong><?php echo $this->localization->profile('company_name') ?></strong>&nbsp;&nbsp;&nbsp;<?php echo $this->localization->profile('address') ?>
                </footer>


            </div>
        </div>
            </div>


    </div><!-- /.box-body -->
</div><!-- /.box -->





