<div class="row">
            <div class="col-md-4">
                <label>Seleccione la talla</label>
            </div>
            <div class="col-md-8">
                <select name="productsize">
                    <option value="0" disabled>Tallas</option>
                    <option value="xs">XS</option>
                    <option value="s">S</option>
                    <option value="m">M</option>
                    <option value="l">L</option>
                    <option value="xl">XL</option>
                    <option value="0" disabled>Tallas pantalón niño</option>
                    <option value="4">4</option>
                    <option value="6">6</option>
                    <option value="8">8</option>
                    <option value="10">10</option>
                    <option value="12">12</option>
                    <option value="0" disabled>Tallas pantalón adulto</option>
                    <option value="30">30</option>
                    <option value="31">31</option>
                    <option value="32">32</option>
                    <option value="33">33</option>
                    <option value="34">34</option>
                    <option value="36">36</option>
                </select>
            </div>
        </div>