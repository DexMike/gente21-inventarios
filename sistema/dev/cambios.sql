--Agregar menu de cuentas
INSERT INTO `tbl_menu` (`menu_id`, `label`, `link`, `icon`, `parent`, `sort`) 
VALUES (NULL, 'Cuentas', '#', 'glyphicon glyphicon-th-large', '0', '21');

--agregar categorías dentro de cuentas
INSERT INTO `tbl_menu` (`menu_id`, `label`, `link`, `icon`, `parent`, `sort`) 
VALUES (NULL, 'Cuentas por Cobrar', 'admin/accounts/receivable', 'fa fa-bar-chart', '44', '1'), (NULL, 'Cuentas por pagar', 'admin/accounts/payable', 'fa fa-globe', '44', '2');

--generar tabla de cuentas por pagar/cobrar
DROP TABLE IF EXISTS `tbl_acounts`;
CREATE TABLE IF NOT EXISTS `tbl_acounts` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `invoice_id` int(12) NOT NULL,
  `date` datetime NOT NULL,
  `supplier_id` int(1) NOT NULL,
  `value` decimal(12,2) NOT NULL DEFAULT '0.00',
  `due_date` datetime NOT NULL,
  `notes` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--cambia la columna de fecha a ser timestamp
ALTER TABLE `tbl_acounts` CHANGE `date` `date_entered` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;

-- agregar columna de fecha de cambios
ALTER TABLE `tbl_acounts` ADD `date_modified` DATETIME on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `date_entered`;

--claro que accounts lleva doble c
ALTER TABLE `tbl_acounts` CHANGE "tbl_acounts" "tbl_accounts";

-- consistencia con las columnas de fecha
ALTER TABLE `tbl_accounts` CHANGE `due_date` `date_due` DATETIME NOT NULL;

-- flag de cobrar o pagar
ALTER TABLE `tbl_accounts` ADD `is_payable` INT(1) NOT NULL DEFAULT '0' 
	COMMENT 'is_payable es una cuenta por pagar, de otra forma es por cobrar' AFTER `id`;

-- no usar mayúsculas dentro de la frase (en español no se usa)
UPDATE `tbl_menu` SET `label` = 'Cuentas por cobrar' WHERE `tbl_menu`.`menu_id` = 45;

-- ícono de cuentas por cobrar
UPDATE `tbl_menu` SET `icon` = 'fa fa-money' WHERE `tbl_menu`.`menu_id` = 45;

--cuentas por pagar
UPDATE `tbl_menu` SET `icon` = 'fa fa-book' WHERE `tbl_menu`.`menu_id` = 46;

--menu agregar cuenta
INSERT INTO `tbl_menu` (`menu_id`, `label`, `link`, `icon`, `parent`, `sort`) 
VALUES (NULL, 'Agregar cuenta', 'admin/accounts/add_account', 'glyphicon glyphicon-plus', '44', '0');

--defult a cero en invoice ID
ALTER TABLE `tbl_accounts` CHANGE `invoice_id` `invoice_id` INT(12) NOT NULL DEFAULT '0';

--notes debe ser nulo
ALTER TABLE `tbl_accounts` CHANGE `notes` `notes` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL;



----------------------------------------------------------------------------------------------------------------------------

--accounts payable is now two different things
UPDATE `tbl_menu` SET `link` = 'admin/accounts/add_account_payable' WHERE `tbl_menu`.`menu_id` = 47;

UPDATE `tbl_menu` SET `label` = 'Agregar cuenta por pagar' WHERE `tbl_menu`.`menu_id` = 47;

ALTER TABLE `tbl_accounts` CHANGE `supplier_id` `supplier_customer_id` INT(1) NOT NULL;

INSERT INTO `tbl_menu` (`menu_id`, `label`, `link`, `icon`, `parent`, `sort`) 
VALUES (NULL, 'Agregar cuenta por cobrar', 'admin/accounts/add_account_receivable', 'glyphicon glyphicon-plus', '44', '1');

---------------------------

ALTER TABLE `tbl_purchase` CHANGE `purchase_order_number` `purchase_order_number` INT(11) NULL;