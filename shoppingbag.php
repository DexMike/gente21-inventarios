﻿<?php

// INCLUDE JCART BEFORE SESSION START
include 'jcart/jcart.php';

// START SESSION
session_start();

// INITIALIZE JCART AFTER SESSION START
$cart =& $_SESSION['jcart']; if(!is_object($cart)) $cart = new jcart();

// IF JAVASCRIPT IS DISABLE SHOPPING CART WILL WORK ANYWAY
$cart->process_cart_nonjs($jcart);

// LET'S GET FILENAME AND MAKE IT VARIABLE
$filename = $_SERVER['REQUEST_URI'];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
	
	<title>Shopping Bag - boutique</title>
	
	<link rel="stylesheet" href="css/default.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="css/fancybox.css" type="text/css" media="screen" />
	<!--[if IE 7]><link rel="stylesheet" href="css/ie7-fix.css" type="text/css" media="screen" /><![endif]-->
	<!--[if gt IE 7]><link rel="stylesheet" href="css/ie8-fix.css" type="text/css" media="screen" /><![endif]-->
</head>
<body id="shoppingBag">
<div id="container">

<!--[if IE]><div class="m-tl"></div><div class="m-tr"></div><![endif]-->

	<div id="header">

		<h1><a href="index.php" title="boutique">boutique</a></h1>
		
		<div id="details">

			<span class="details"><?php $cart->total_items($jcart);?>&nbsp;<span class="topDivider">|</span>&nbsp;<?php $cart->total_price($jcart);?></span>

			<a href="shoppingbag.php" title="Shopping Bag"><span class="shoppingBag">Shopping Bag</span></a>
			<a href="shoppingbag.php" title="Checkout"><span class="checkout">Checkout</span></a>
			
		</div><!-- end details -->

	</div><!-- end header -->

	<div id="content">

		<div id="categoriesBar">

			<ul>
				<li><a href="index.php">Home</a> <span class="navDivider">//</span></li>
				<li><a href="about.php">About</a> <span class="navDivider">//</span></li>
				<li><a href="#">Shipping</a> <span class="navDivider">//</span></li>
				<li><a href="#">FAQs</a> <span class="navDivider">//</span></li>
				<li><a href="contact.php">Contact</a></li>
			</ul>

		</div><!-- end categoriesBar -->

		<div id="main">

			<h2>Shopping Bag</h2>
			
			<?php $cart->display_cart($jcart);?>
			
		</div><!-- end main -->

	</div><!-- end content -->
	
<!--[if IE]><div class="m-bl"></div><div class="m-br"></div><![endif]-->

</div><!-- end container -->

<div id="footer">

<!--[if IE]><div class="f-tl"></div><div class="f-tr"></div><![endif]-->

	<div id="navigation">

		<ul>
			<li><a href="index.php">Home</a> <span class="navDivider">//</span></li>
			<li><a href="about.php">About</a> <span class="navDivider">//</span></li>
			<li><a href="#">Shipping</a> <span class="navDivider">//</span></li>
			<li><a href="#">FAQs</a> <span class="navDivider">//</span></li>
			<li><a href="contact.php">Contact</a></li>
		</ul>

	</div><!-- end navigation -->

	<div id="rssFeed">

		<span>
			<a class="image" href="#">
				<img width="16" height="16" alt="New Items RSS Feed" src="img/rss.png" class="rssIcon" />
			</a>
			<a href="#">New Items RSS Feed</a>
		</span>

	</div><!-- end rssFeed -->

	<div id="copyright">

		<p class="right">Copyright &copy; Your name here. All rights reserved</p>

	</div><!-- end copyright -->
	
<!--[if IE]><div class="f-bl"></div><div class="f-br"></div><![endif]-->

</div><!-- end footer -->

<!--[if IE]><div class="clearBottom">&nbsp;</div><![endif]-->

<!--start scripts-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script type="text/javascript" src="jcart/jcart-javascript.php"></script>
<script type="text/javascript" src="js/jquery.fancybox-1.3.1.pack.js"></script>
<script type="text/javascript" src="js/jquery.quicksearch.js"></script>
<script type="text/javascript" src="js/custom.js"></script>
<!--end scripts-->
</body>
</html>