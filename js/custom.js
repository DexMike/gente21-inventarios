var timeout = null;

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

function removeParamsFromBrowserURL(){
    return window.location.href.replace(/\?.*/,'');
}

(function($){
    $("document").ready(function(){

    	var products, count, paginator, subcatID, pagina, buscar;

    	//barra de categorías
    	$.ajax({
            type: 'post',
            url: 'getCategories.php',
            dataType : 'text',
            success: function(response) {
                navCategories = response;
            },
            async: false
        });
        var ulCategories = '<div class="col-md-9"><div class="subcatnav"><ul id="categories"> ' +
	            navCategories+' </ul></div></div>';
	    var $location = $('#searchdiv');
	    $location.before(ulCategories);

	    //checar si tenemos categoría en URL
    	if (getUrlParameter('subcatID') > 0) {	
        	subcatID = getUrlParameter('subcatID');
        } else {
        	subcatID = 0; //todos los productos
        }

        //checa qué pagina es
        if (getUrlParameter('pagina') > 0) {
        	pagina = getUrlParameter('pagina');
        } else {
        	pagina = 1;
        }

        //checa si hay parametro 'buscar' en URL
        if (getUrlParameter('buscar') == null) {
        	buscar = "";
        } else {
        	buscar = getUrlParameter('buscar');
        }

        $.ajax({
            type: 'post',
            url: 'getProducts.php',
            data: {
            	subcatID: subcatID,
            	pagina: pagina,
            	buscar: buscar
            },
            dataType : 'json',
            success: function(response) {
                products = response['products'];
                count = response['count'];
                paginator = response['paginator'];
                subcat = response['subcat'];
                subcatID = response['subcatID'];
            	console.log("**testing**");
            	console.log("subcat id: "+response['subcatID']);
                console.log("subcat: "+response['subcat']);
                //console.log("products: "+response['products']);
                //console.log("paginator: "+response['paginator']);
                console.log("buscar: "+response['buscar']);
                console.log("pagina: "+pagina);
                console.log("count: "+response['count']);
                //console.log("sql: "+response['sql']);
                console.log("debugo: "+response['debugo']);
            },
            async: false
        }); 

        if (subcatID == 0) {
        	$('#pagePanelTop span.numItems, #pagePanelBottom span.numItems').html('<span class="titleItems">todos los productos</span> (Total: ' + count + ')');
        	$('.titleItems').css("text-transform","capitalize");
        } else {
        	$('#pagePanelTop span.numItems, #pagePanelBottom span.numItems').html('<span class="titleItems">' + subcat + '</span> (Total: ' + count + ')');
        	$('.titleItems').css("text-transform","capitalize");
        }

        //show products
        $('#items').html(products);

        //get paginator
        $('#paginator').html(paginator);



   	});
})(jQuery);

// Zoom Icon
(function($){
    $("document").ready(function(){
        $('#home img.itemImage').before('<img src="img/zoom-icon.png" width="190" height="190" alt="Clic para agrandar imagen" class="zoomImage" />');
        $('#home img.zoomImage').hover(function(){
            $(this).stop().animate({opacity:0.8},400);
            },function(){
            $(this).stop().animate({opacity:0},400);
            });
    });
})(jQuery);

// "Lightbox"
(function($){
    $("document").ready(function(){
        $("a.single_image").fancybox({
            'transitionIn'  : 'elastic',
            'transitionOut' : 'elastic'
        });
        $("a.multi_image").attr('rel', 'gallery').fancybox({
            'transitionIn'  : 'elastic',
            'transitionOut' : 'elastic'
        });
    });
})(jQuery);

// Page Scroller
(function($){
    $("document").ready(function(){
        $('.scrollPage').click(function() {
            var elementClicked = $(this).attr("href");
            var destination = $(elementClicked).offset().top;
            $("html:not(:animated),body:not(:animated)").animate({ scrollTop: destination-20}, 500 );
            return false;
        });
    });
})(jQuery);

// Slideshow
(function($){
    $("document").ready(function(){
        $('#slideshow').cycle({ 
            speed:  '500', 
            timeout: 0, 
            pager:  '#nav', 
            pagerAnchorBuilder: function(idx, slide) { 
                // return selector string for existing anchor 
                return '#nav li:eq(' + idx + ') a'; 
            } 
        });
    });
})(jQuery);