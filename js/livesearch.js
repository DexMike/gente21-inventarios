// jQuery Live Search
(function($){
	$("document").ready(function(){
		$('div#items div.item').quicksearch({
			attached: 'div#search',
			position: 'prepend',
			labelText: '',
			inputText: 'Búsqueda...',
			loaderImg: 'img/ajax-loader.gif',
			delay: '150'
		});	
	});
})(jQuery);
