﻿<?php
// INCLUDE JCART BEFORE SESSION START
include 'jcart/jcart.php';

// START SESSION
session_start();

// INITIALIZE JCART AFTER SESSION START
$cart =& $_SESSION['jcart']; if(!is_object($cart)) $cart = new jcart();

// IF JAVASCRIPT IS DISABLE SHOPPING CART WILL WORK ANYWAY
$cart->process_cart_nonjs($jcart);

// LET'S GET FILENAME AND MAKE IT VARIABLE
$filename = $_SERVER['REQUEST_URI'];

include 'conexion.php';

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
	
	<title>Botón y Agujeta</title>
	<link href='http://fonts.googleapis.com/css?family=Source+Code+Pro|Open+Sans:300' rel='stylesheet' type='text/css'> 


	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" type="text/css" />
	<link rel="stylesheet" href="css/default.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="css/fancybox.css" type="text/css" media="screen" />

	<!--[if IE 7]><link rel="stylesheet" href="css/ie7-fix.css" type="text/css" media="screen" /><![endif]-->
	<!--[if gt IE 7]><link rel="stylesheet" href="css/ie8-fix.css" type="text/css" media="screen" /><![endif]-->
</head>
<body id="home">
<div class="container">

<!--[if IE]><div class="m-tl"></div><div class="m-tr"></div><![endif]-->

	<div id="header">

		<h1><a href="index.php" title="boutique">boutique</a></h1>
		
		<!--<div id="details">
			
			<span class="details"><?php $cart->total_items($jcart);?>&nbsp;<span class="topDivider">|</span>&nbsp;<?php $cart->total_price($jcart);?></span>
				
			<a href="shoppingbag.php" title="Shopping Bag"><span class="shoppingBag">Shopping Bag</span></a>
			<a href="shoppingbag.php" title="Checkout"><span class="checkout">Checkout</span></a>

		</div> end details -->

	</div><!-- end header -->

	<div id="content">

		<div id="categoriesBar">
			<div class="row">
				<div class="col-md-3" id="searchdiv">
					<div class="row">
						<div class="col-md-2">
							<img src="img/ajax-loader.gif" alt="Loading" id="loader" class="loader" style="display: none;">
						</div>
						<div class="col-md-10">
							<div id="search">
							<form action="index.php" method="GET" id="quicksearch" class="quicksearch">
								<div class="input-group">
									<?php if (isset($_GET['buscar'])): ?>
										<input type="text" name="buscar" placeholder="Búsqueda..." class="qs_input" id="searchBox" value="<?php echo $_GET['buscar']; ?>">
									<?php else: ?>
										<input type="text" name="buscar" placeholder="Búsqueda..." class="qs_input" id="searchBox">
									<?php endif ?>
									
									<span class="input-group-btn"><button type="submit" class="btn btn-primary search-btn" data-target="#search-form" name="q"><span class="glyphicon glyphicon-search"></span></button></span>
								</div>
							</form>
							</div>
						</div>
					</div>

    </div>
</form>


					
					<!-- end search -->
				</div>
			</div>
		</div><!-- end categoriesBar -->

		<div id="pagePanelTop">

			<div class="row">
			
				<div class="col-md-9">
					<p class="left">Mostrando <span class="titleItems"></span> <span class="numItems"></span></p>
				</div>

				<div class="col-md-3" id="searchdiv">
					<div class="pageButtons">
						<span class="text hidden-xs">Ir abajo &raquo;</span>
						<a href="#footer" title="Ir abajo" class="scrollPage"><span class="pageDown">Abajo</span></a>
					</div><!-- end pageButtons -->
				</div>

			</div>
			
		</div><!-- end pagePanelTop -->


		<div id="items"></div><!-- productos -->
			

		<div id="pagePanelBottom">

			<div class="row">
				<div class="col-md-10">
					<div id="paginator"></div>
				</div>
				<div class="col-md-2">
					<div class="pageButtons">
						<span class="text hidden-xs">Ir Arriba &raquo;</span>
						<a href="#header" title="Ir Arriba" class="scrollPage"><span class="pageUp">Arriba</span></a>
					</div><!-- end pageButtons -->
				</div>
			</div>

		</div><!-- end pagePanelBottom -->

	</div><!-- end content -->
	
<!--[if IE]><div class="m-bl"></div><div class="m-br"></div><![endif]-->

</div><!-- end container -->

<div id="footer" class="container">

	<div id="copyright">

		<p class="right">&copy; DERECHOS RESERVADOS <?php echo date('Y') ?> | Desarrollo Web: GoAdmin</p>

	</div><!-- end copyright -->

</div><!-- end footer -->

<!--start scripts-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="jcart/jcart-javascript.php"></script>
<script type="text/javascript" src="js/jquery.fancybox-1.3.1.pack.js"></script>
<script type="text/javascript" src="js/custom.js"></script>
<!--end scripts-->
</body>
</html>