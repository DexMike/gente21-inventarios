<?php

// JCART v1.1
// http://conceptlogic.com/jcart/

// THIS FILE TAKES INPUT FROM AJAX REQUESTS VIA JQUERY post AND get METHODS, THEN PASSES DATA TO JCART
// RETURNS UPDATED CART HTML BACK TO SUBMITTING PAGE

// INCLUDE JCART BEFORE SESSION START
include_once 'jcart.php';

// START SESSION
session_start();

// INITIALIZE JCART AFTER SESSION START
$cart =& $_SESSION['jcart']; if(!is_object($cart)) $cart = new jcart();

// PROCESS INPUT AND RETURN UPDATED CART HTML
if ($_POST['show_cart_details']){

	$cart->total_items($jcart);
	echo '&nbsp;<span class="topDivider">|</span>&nbsp;';
	$cart->total_price($jcart);
	}
else
	{
	$cart->display_cart($jcart);
}

?>
