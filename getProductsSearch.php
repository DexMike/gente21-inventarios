﻿<?php
include 'conexion.php';

$data = "";
$paginator = "";
$limit = 50;

$start_from = ($_POST["pagina"]-1) * $limit;

$sqlCount = "SELECT tbl_product.product_id AS product_id, 
    tbl_product.product_code AS code, 
    tbl_product.product_name AS name, 
    group_concat(tbl_product.product_code) AS sizes 
FROM tbl_product 
JOIN tbl_subcategory ON tbl_subcategory.subcategory_id = tbl_product.subcategory_id 
INNER JOIN tbl_product_tag ON tbl_product_tag.product_id = tbl_product.product_id 
WHERE status = 1 AND tbl_product.product_name LIKE '%".$_POST['buscar']."%' OR tbl_product.product_code LIKE '%".$_POST['buscar']."%' GROUP BY tag"; 

$result = $con->query($sqlCount);

if ($result->num_rows > 0) {
    $count = $result->num_rows;
} else {
    $count = 0;
}

$sql = "SELECT tbl_product.product_id AS product_id, 
    tbl_product.product_code AS code, 
    tbl_product.product_name AS name, 
    tbl_category.category_name AS category, 
    tbl_subcategory.subcategory_name AS subcategory, 
    tbl_subcategory.subcategory_id AS subcategoryID, 
    tbl_inventory.product_quantity AS quantity, 
    tbl_product_price.selling_price AS price, 
    tbl_product_image.filename AS filename, 
    tbl_product_tag.tag AS tag, 
    group_concat(tbl_inventory.product_quantity) AS quantities, 
    group_concat(tbl_product.product_code) AS sizes 
FROM tbl_product 
JOIN tbl_subcategory ON tbl_subcategory.subcategory_id = tbl_product.subcategory_id 
INNER JOIN tbl_category ON tbl_category.category_id = tbl_category.category_id 
INNER JOIN tbl_inventory ON tbl_inventory.product_id = tbl_product.product_id 
INNER JOIN tbl_product_price ON tbl_product_price.product_id = tbl_product.product_id 
INNER JOIN tbl_product_tag ON tbl_product_tag.product_id = tbl_product.product_id 
LEFT JOIN tbl_product_image ON tbl_product_image.product_id = tbl_product.product_id 
WHERE status = 1 AND tbl_product.product_name LIKE '%".$_POST['buscar']."%' OR tbl_product.product_code LIKE '%".$_POST['buscar']."%' GROUP BY tag LIMIT $start_from, $limit";

$result = $con->query($sql);

if ($result->num_rows > 0) {

    while($line = $result->fetch_assoc()) {
        $subcat = $line['subcategory'];

        ob_start();
    ?>
        <div class="item">
            <?php if ($line['filename'] != ""): //sí hay imagen ?>
                <a class="single_image" title="<?php echo $line['name'] ?>" href="sistema/<?php echo $line['filename'] ?>">
                <img src="sistema/<?php echo $line['filename'] ?>" width="190" height="190" alt="<?php echo $line['name'] ?>" class="itemImage" />
            <?php else: //no hay imagen ?>
                <a class="single_image" title="<?php echo $line['name'] ?>" href="img/sample_images/sampleImage.png">
                <img src="img/sample_images/sampleImage.png" width="190" height="190" alt="<?php echo $line['name'] ?>" class="itemImage" />
            <?php endif; ?>
                <!--<img src="img/new.png" alt="New" class="itemNew" />-->
            </a>
            <p class="itemName"><a href="#"><?php echo $line['name'] ?></a></p>
            <p class="itemPrice">$<?php echo number_format($line['price'], 2, '.', ','); ?></p>
            <?php 
                $sizesName = explode(",", $line['sizes']); //obtenemos array de tallas
                $sizesQuantity = explode(",", $line['quantities']); //obtenemos array de cantidades
                $sizes = "";
                $code = "";

                //imprimimos el código del producto, sin las letras de la Talla:
                if (substr($line['code'], -2, 1) == "X") {
                    $code = substr($line['code'], 0, -2);
                } else {
                    $code = substr($line['code'], 0, -1);
                }
                echo '<strong>'.$code.'</strong><br>';

                //imprimimos las tallas, con sus respectivas cantidades:
                foreach (array_combine($sizesName, $sizesQuantity) as $size => $quantity) {
                    if (substr($size, -2, 1) == "X") {
                        $size = substr($size, -2);
                    } else if ( (substr($size, -2, 1) != "X") && (substr($size, -1) == "L" || substr($size, -1) == "M" || substr($size, -1) == "S")){
                        $size = substr($size, -1);
                    } else if (is_numeric(substr($size, -1))) {
                        if (substr($size, -2, 1) == "3") {
                            $size = "Talla ".substr($size, -2);
                        } else {
                            $size = "Talla ".substr($size, -1);
                        }
                    } else {
                        $size = substr($size, -1);
                    }
                    $sizes .= $size.":".$quantity." | ";
                }
                $sizes = substr($sizes, 0, -3); 
                echo $sizes; 
            ?>
        </div><!-- end item -->
    <?php 
        $data .= ob_get_clean();

    } 
} else {
	$data .= "No hay resultados";
}

$total_pages = ceil($count / $limit);
$paginator .= '<ul class="pagination text-center">';
if(!empty($total_pages)){
    for($i=1; $i<=$total_pages; $i++){
        if($_POST["pagina"] == $i){
            $paginator .= '<li class="active"  id="'.$i.'"><a href="?buscar='.$_POST['buscar'].'&pagina='.$i.'">'.$i.'</a></li> ';
        } else {
            $paginator .= '<li id="'.$i.'"><a href="?buscar='.$_POST['buscar'].'&pagina='.$i.'">'.$i.'</a></li>';
        }
    }
}

$paginator .= '</ul>';

$response = array(
    'products' => $data,
    'count' => $count,
    'paginator' => $paginator,
    'pagina' => $_POST["pagina"],
    'buscar' => $_POST['buscar'],
    'query' => $sql
);

$con->close();

echo json_encode($response);
?>