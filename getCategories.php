﻿<?php
include 'conexion.php';

$sql = "SELECT DISTINCT subcategory_name AS subcategory, subcategory_id AS id FROM tbl_subcategory WHERE category_id = 1";
$result = $con->query($sql);

$data = "";
$category = array();

$unwanted_array = array(    'Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
                            'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U',
                            'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c',
                            'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o',
                            'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y' );

$data .= '<li><a href="?subcatID=0" class="all" id="subcat_0">Todos los productos</a><span class="navDivider"></span></li> ';

if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        $category = (str_replace(' ', '_', strtolower($row["subcategory"]))); //para clase HTML, quitamos espacios y ponemos en minusculas
        //$category = (str_replace('ñ', 'n', strtolower($category)));
        $category = strtr(($category), $unwanted_array);
        $data .= '<li><a href="index.php?subcatID='.$row["id"].'" class="'.$category.'" id="subcat_'.$row["id"].'">'.$row["subcategory"].'</a><span class="navDivider"></span></li> ';
    }
} else {
	$data .= "0 results";
    echo "0 results";
}

//$data = substr($data, 0, -40); //quitamos último divider
$data .= '</li>';

$con->close();

echo $data;
?>