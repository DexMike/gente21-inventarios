<?php

// Email address where message are going to be sen
$your_email = 'sandbox@mymail.fi'; // Replace this with your own email address

//Ensures no one loads page and does simple spam check.
if(isset($_POST['name']) && empty($_POST['spam_check']))
{
	
	//Include our email validator for later use 
	require 'email-validator.php';
	$validator = new EmailAddressValidator();
	
	//Setup our basic variables
	$input_name = strip_tags($_POST['name']);
	$input_email = strip_tags($_POST['email']);
	$input_subject = strip_tags($_POST['subject']);
	$input_message = strip_tags($_POST['message']);
	
	//We'll check and see if any of the required fields are empty.
	if (strlen($input_name) < 2) {
		$error['name'] = "Please enter your name.";
	}

	if (strlen($input_message) < 5) {
		$error['comments'] = "Please leave a comment.";
	}
	
	//Make sure the email is valid.
	if (!$validator->check_email_address($input_email)) {
		$error['email'] = "Please enter a valid email address.";
	}

	// Default subject if subject field has left empty
	if (strlen($input_subject) == 0) {
		$input_subject = 'Contact Form';
	}
	
	//Now check to see if there are any errors 
	if(!$error)
	{
		//No errors, send mail using conditional to ensure it was sent.
		if(mail($your_email, "Message from $input_name - $input_subject", $input_message . "\n\n---\nThis email is sent by contact form.", "From: $input_email"))
		{
			echo '<span class="green">Your email has been sent!</span>';
		}
		else 
		{
			echo '<span class="red">There was a problem sending your email!</span>';
		}
		
	}
	else 
	{
		
		//Errors were found, output all errors to the user.
		$response = (isset($error['name'])) ? "<span class='red'>" . $error['name'] . "</span><br /> \n" : null;
		$response .= (isset($error['email'])) ? "<span class='red'>" . $error['email'] . "</span><br /> \n" : null;
		$response .= (isset($error['comments'])) ? "<span class='red'>" . $error['comments'] . "</span><br /> \n" : null;

		echo $response;
		
	}
}
else
{
	die('Direct access to this page is not allowed.');
}